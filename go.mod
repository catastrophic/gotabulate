module gitlab.com/catastrophic/gotabulate

require (
	github.com/mattn/go-runewidth v0.0.4
	github.com/nsf/termbox-go v0.0.0-20190104133558-0938b5187e61
	github.com/stretchr/testify v1.3.0
)
